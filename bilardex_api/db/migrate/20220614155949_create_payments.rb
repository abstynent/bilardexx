class CreatePayments < ActiveRecord::Migration[7.0]
  def change
    create_table :payments do |t|
      t.boolean :is_paid
      t.float :price

      t.timestamps
    end
  end
end
