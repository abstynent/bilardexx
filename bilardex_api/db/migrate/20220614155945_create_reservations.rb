class CreateReservations < ActiveRecord::Migration[7.0]
  def change
    create_table :reservations do |t|
      t.integer :pool_table_id
      t.integer :user_id
      t.timestamp :start_time
      t.timestamp :end_time
      t.integer :payment_id

      t.timestamps
    end
  end
end
