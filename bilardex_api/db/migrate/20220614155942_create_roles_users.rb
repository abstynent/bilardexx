class CreateRolesUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :roles_users, id: false do |t|
      t.belongs_to :roles, index: true
      t.belongs_to :users, index: true
    end
  end
end
