class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :tel_number
      t.string :email
      t.string :password_digest
      t.boolean :premium
      t.string :session_cookie

      t.timestamps
    end
  end
end
