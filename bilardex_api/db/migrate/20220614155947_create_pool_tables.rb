class CreatePoolTables < ActiveRecord::Migration[7.0]
  def change
    create_table :pool_tables do |t|
      t.float :position_x
      t.float :position_y
      t.string :table_type
      t.float :hourly_price

      t.timestamps
    end
  end
end
