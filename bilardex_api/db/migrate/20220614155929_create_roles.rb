class CreateRoles < ActiveRecord::Migration[7.0]
  def change
    create_table :roles do |t|
      t.string :role_name
      t.boolean :user_read
      t.boolean :user_write
      t.boolean :reservation_read
      t.boolean :reservation_write
      t.boolean :pool_table_read
      t.boolean :pool_table_write
      t.boolean :payment_read
      t.boolean :payment_write
      t.boolean :api_permissions_write
      t.boolean :api_permissions_read

      t.timestamps
    end
  end
end
