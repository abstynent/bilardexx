Rails.application.routes.draw do
  resources :payments
  resources :pool_tables
  resources :reservations
  resources :users
  resources :roles

  root to: 'static#index'

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get    '/logout',  to: 'sessions#destroy'

  #get    '/reserve'  to: 'reservation_write#new'
  #get    '/reserve'  to: 'reservations#index'

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
