class StaticController < ApplicationController
  def index
    @pool_tables=PoolTable.all
    @payments=Payment.all
    @users=User.all
    @reservations=Reservation.all

    @user=User.first

    @pool_tables_free=PoolTable.all.to_a

    #@time_start="2022-06-10 00:00:00 UTC"
    #@time_end="2022-06-10 00:01:00 UTC"
    @time_start=Time.now
    @time_end=Time.now+3600

    @reservation_active=[]
    @reservations.each do |reservation|
      if (reservation.start_time < @time_start and reservation.end_time > @time_start)
        @reservation_active.append(reservation)
      end
    end

    @reservation_in_time_range=[]
    @reservations.each do |reservation|
      if !((reservation.start_time < @time_start and reservation.end_time < @time_start)or(reservation.start_time > @time_end and reservation.end_time > @time_end))
        @reservation_in_time_range.append(reservation)
      end
    end

    @reservations.each do |reservation|
      if !((reservation.start_time < @time_start and reservation.end_time < @time_start)or(reservation.start_time > @time_end and reservation.end_time > @time_end))
        @index=0
        @pool_tables_free.each do |ptf|
          if ptf == reservation.pool_table
            @pool_tables_free.delete_at(@index)
          end
          @index=@index+1;
        end
      end
    end

  end
end
