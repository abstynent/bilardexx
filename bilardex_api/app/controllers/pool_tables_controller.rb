class PoolTablesController < ApplicationController
  before_action :set_pool_table, only: %i[ show edit update destroy ]

  # GET /pool_tables or /pool_tables.json
  def index
    @pool_tables = PoolTable.all
  end

  # GET /pool_tables/1 or /pool_tables/1.json
  def show
  end

  # GET /pool_tables/new
  def new
    @pool_table = PoolTable.new
  end

  # GET /pool_tables/1/edit
  def edit
  end

  # POST /pool_tables or /pool_tables.json
  def create
    @pool_table = PoolTable.new(pool_table_params)

    respond_to do |format|
      if @pool_table.save
        format.html { redirect_to pool_table_url(@pool_table), notice: "Pool table was successfully created." }
        format.json { render :show, status: :created, location: @pool_table }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @pool_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pool_tables/1 or /pool_tables/1.json
  def update
    respond_to do |format|
      if @pool_table.update(pool_table_params)
        format.html { redirect_to pool_table_url(@pool_table), notice: "Pool table was successfully updated." }
        format.json { render :show, status: :ok, location: @pool_table }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @pool_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pool_tables/1 or /pool_tables/1.json
  def destroy
    @pool_table.destroy

    respond_to do |format|
      format.html { redirect_to pool_tables_url, notice: "Pool table was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pool_table
      @pool_table = PoolTable.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def pool_table_params
      params.require(:pool_table).permit(:position_x, :position_y, :table_type, :hourly_price)
    end
end
