class User < ApplicationRecord
  has_and_belongs_to_many :roles, :join_table => :roles_users
  has_many :reservations
  validates :email, presence: true, uniqueness: true, length: { in: 3..50 }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }

end
