class Role < ApplicationRecord
  has_and_belongs_to_many :user, :join_table => :roles_users
end
