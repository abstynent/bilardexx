class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :pool_table
  belongs_to :payment
end
