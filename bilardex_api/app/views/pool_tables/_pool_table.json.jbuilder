json.extract! pool_table, :id, :position_x, :position_y, :table_type, :hourly_price, :created_at, :updated_at
json.url pool_table_url(pool_table, format: :json)
