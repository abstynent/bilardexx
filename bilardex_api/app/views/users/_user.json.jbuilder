json.extract! user, :id, :name, :tel_number, :email, :password_digest, :premium, :session_cookie, :created_at, :updated_at
json.url user_url(user, format: :json)
