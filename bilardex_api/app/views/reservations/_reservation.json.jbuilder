json.extract! reservation, :id, :pool_table_id, :user_id, :start_time, :end_time, :payment_id, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
