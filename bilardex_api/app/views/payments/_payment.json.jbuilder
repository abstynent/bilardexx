json.extract! payment, :id, :is_paid, :price, :created_at, :updated_at
json.url payment_url(payment, format: :json)
