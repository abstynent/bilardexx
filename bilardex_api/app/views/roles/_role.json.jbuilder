json.extract! role, :id, :role_name, :user_read, :user_write, :reservation_read, :reservation_write, :pool_table_read, :pool_table_write, :payment_read, :payment_write, :api_permissions_write, :api_permissions_read, :created_at, :updated_at
json.url role_url(role, format: :json)
