require "test_helper"

class RolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @role = roles(:one)
  end

  test "should get index" do
    get roles_url
    assert_response :success
  end

  test "should get new" do
    get new_role_url
    assert_response :success
  end

  test "should create role" do
    assert_difference("Role.count") do
      post roles_url, params: { role: { api_permissions_read: @role.api_permissions_read, api_permissions_write: @role.api_permissions_write, payment_read: @role.payment_read, payment_write: @role.payment_write, pool_table_read: @role.pool_table_read, pool_table_write: @role.pool_table_write, reservation_read: @role.reservation_read, reservation_write: @role.reservation_write, role_name: @role.role_name, user_read: @role.user_read, user_write: @role.user_write } }
    end

    assert_redirected_to role_url(Role.last)
  end

  test "should show role" do
    get role_url(@role)
    assert_response :success
  end

  test "should get edit" do
    get edit_role_url(@role)
    assert_response :success
  end

  test "should update role" do
    patch role_url(@role), params: { role: { api_permissions_read: @role.api_permissions_read, api_permissions_write: @role.api_permissions_write, payment_read: @role.payment_read, payment_write: @role.payment_write, pool_table_read: @role.pool_table_read, pool_table_write: @role.pool_table_write, reservation_read: @role.reservation_read, reservation_write: @role.reservation_write, role_name: @role.role_name, user_read: @role.user_read, user_write: @role.user_write } }
    assert_redirected_to role_url(@role)
  end

  test "should destroy role" do
    assert_difference("Role.count", -1) do
      delete role_url(@role)
    end

    assert_redirected_to roles_url
  end
end
