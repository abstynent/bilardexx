require "test_helper"

class PoolTablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pool_table = pool_tables(:one)
  end

  test "should get index" do
    get pool_tables_url
    assert_response :success
  end

  test "should get new" do
    get new_pool_table_url
    assert_response :success
  end

  test "should create pool_table" do
    assert_difference("PoolTable.count") do
      post pool_tables_url, params: { pool_table: { hourly_price: @pool_table.hourly_price, position_x: @pool_table.position_x, position_y: @pool_table.position_y, table_type: @pool_table.table_type } }
    end

    assert_redirected_to pool_table_url(PoolTable.last)
  end

  test "should show pool_table" do
    get pool_table_url(@pool_table)
    assert_response :success
  end

  test "should get edit" do
    get edit_pool_table_url(@pool_table)
    assert_response :success
  end

  test "should update pool_table" do
    patch pool_table_url(@pool_table), params: { pool_table: { hourly_price: @pool_table.hourly_price, position_x: @pool_table.position_x, position_y: @pool_table.position_y, table_type: @pool_table.table_type } }
    assert_redirected_to pool_table_url(@pool_table)
  end

  test "should destroy pool_table" do
    assert_difference("PoolTable.count", -1) do
      delete pool_table_url(@pool_table)
    end

    assert_redirected_to pool_tables_url
  end
end
