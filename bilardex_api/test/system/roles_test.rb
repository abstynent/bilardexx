require "application_system_test_case"

class RolesTest < ApplicationSystemTestCase
  setup do
    @role = roles(:one)
  end

  test "visiting the index" do
    visit roles_url
    assert_selector "h1", text: "Roles"
  end

  test "should create role" do
    visit roles_url
    click_on "New role"

    check "Api permissions read" if @role.api_permissions_read
    check "Api permissions write" if @role.api_permissions_write
    check "Payment read" if @role.payment_read
    check "Payment write" if @role.payment_write
    check "Pool table read" if @role.pool_table_read
    check "Pool table write" if @role.pool_table_write
    check "Reservation read" if @role.reservation_read
    check "Reservation write" if @role.reservation_write
    fill_in "Role name", with: @role.role_name
    check "User read" if @role.user_read
    check "User write" if @role.user_write
    click_on "Create Role"

    assert_text "Role was successfully created"
    click_on "Back"
  end

  test "should update Role" do
    visit role_url(@role)
    click_on "Edit this role", match: :first

    check "Api permissions read" if @role.api_permissions_read
    check "Api permissions write" if @role.api_permissions_write
    check "Payment read" if @role.payment_read
    check "Payment write" if @role.payment_write
    check "Pool table read" if @role.pool_table_read
    check "Pool table write" if @role.pool_table_write
    check "Reservation read" if @role.reservation_read
    check "Reservation write" if @role.reservation_write
    fill_in "Role name", with: @role.role_name
    check "User read" if @role.user_read
    check "User write" if @role.user_write
    click_on "Update Role"

    assert_text "Role was successfully updated"
    click_on "Back"
  end

  test "should destroy Role" do
    visit role_url(@role)
    click_on "Destroy this role", match: :first

    assert_text "Role was successfully destroyed"
  end
end
