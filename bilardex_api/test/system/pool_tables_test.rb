require "application_system_test_case"

class PoolTablesTest < ApplicationSystemTestCase
  setup do
    @pool_table = pool_tables(:one)
  end

  test "visiting the index" do
    visit pool_tables_url
    assert_selector "h1", text: "Pool tables"
  end

  test "should create pool table" do
    visit pool_tables_url
    click_on "New pool table"

    fill_in "Hourly price", with: @pool_table.hourly_price
    fill_in "Position x", with: @pool_table.position_x
    fill_in "Position y", with: @pool_table.position_y
    fill_in "Table type", with: @pool_table.table_type
    click_on "Create Pool table"

    assert_text "Pool table was successfully created"
    click_on "Back"
  end

  test "should update Pool table" do
    visit pool_table_url(@pool_table)
    click_on "Edit this pool table", match: :first

    fill_in "Hourly price", with: @pool_table.hourly_price
    fill_in "Position x", with: @pool_table.position_x
    fill_in "Position y", with: @pool_table.position_y
    fill_in "Table type", with: @pool_table.table_type
    click_on "Update Pool table"

    assert_text "Pool table was successfully updated"
    click_on "Back"
  end

  test "should destroy Pool table" do
    visit pool_table_url(@pool_table)
    click_on "Destroy this pool table", match: :first

    assert_text "Pool table was successfully destroyed"
  end
end
